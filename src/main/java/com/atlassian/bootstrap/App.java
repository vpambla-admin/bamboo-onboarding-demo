package com.atlassian.bootstrap;


import org.jetbrains.annotations.Nullable;

/**
 * Hello world!
 */
public class App
{
    public static void main(String[] args)
    {
        System.out.println("Hello World!");
    }

    @Override
    @Nullable
    public String toString()
    {
        return "hello";
    }
}
